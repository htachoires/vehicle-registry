import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { SearchComponent } from './vehicle/search/search.component';
import { DetailComponent } from './vehicle/detail/detail.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EmailComponent as ForgetPasswordEmail } from './forget-password/email/email.component';
import { TokenComponent as ForgetPasswordTokenComponent } from './forget-password/token/token.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ListComponent as VehicleListComponent } from './vehicle/list/list.component';
import { NavbarComponent } from './navbar/navbar.component';
import { ProfileComponent } from './user/profile.component';
import { InformationComponent } from './user/information/information.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { VehicleFollowedComponent } from './user/vehicle-followed/vehicle-followed.component';
import { ForgetPasswordComponent } from './forget-password/forget-password.component';
import { CommentComponent } from './vehicle/detail/comment/comment.component';
import { SuggestedComponent } from './vehicle/detail/suggested/suggested.component';
import { LogoutComponent } from './logout/logout.component';
import { SearchResultComponent } from './vehicle/search-result/search-result.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    SearchComponent,
    DetailComponent,
    ProfileComponent,
    InformationComponent,
    RegisterComponent,
    LoginComponent,
    ForgetPasswordEmail,
    ForgetPasswordTokenComponent,
    ResetPasswordComponent,
    VehicleFollowedComponent,
    VehicleListComponent,
    CommentComponent,
    SuggestedComponent,
    NavbarComponent,
    SuggestedComponent,
    VehicleListComponent,
    ForgetPasswordComponent,
    LogoutComponent,
    SearchResultComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule,
    FormsModule,
    NgbModule,
    RouterModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
