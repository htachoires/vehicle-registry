import { Component, OnInit } from '@angular/core';
import { Vehicle } from 'src/app/Model/Vehicle';
import { VehicleFollowedService } from 'src/app/service/user/vehicle-followed.service';

@Component({
  selector: 'app-vehicle-followed',
  templateUrl: './vehicle-followed.component.html'
})
export class VehicleFollowedComponent implements OnInit {

  public vehicles: Vehicle[];

  constructor(
    private vehicleFollowedService: VehicleFollowedService
  ) {
  }

  ngOnInit(): void {
    this.vehicleFollowedService.getVehicleFollowed()
      .subscribe(data => this.decodeJson(data.results[0].vehicles.items));
  }

  private decodeJson(data): void {
    this.vehicles = new Array<Vehicle>();
    data.forEach(i => this.vehicles.push(i));
  }

}
