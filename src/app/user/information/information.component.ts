import { Component, Input, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from 'src/app/Model/User';

@Component({
  selector: 'app-information',
  templateUrl: './information.component.html',
  styleUrls: [ './information.component.css' ]
})
export class InformationComponent implements OnInit {

  @Input() userObservable: Observable<any>;

  public user: User;

  constructor() {
  }

  ngOnInit(): void {
    this.userObservable.subscribe(data => this.decodeJson(data.results[0].user));
  }

  private decodeJson(items): void {
    this.user = items;
  }

}
