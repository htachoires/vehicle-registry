import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { UserService } from 'src/app/service/user/user.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html'
})
export class ProfileComponent implements OnInit {

  public userObservable: Observable<any>;

  constructor(private userService: UserService) {
  }

  ngOnInit(): void {
    this.userObservable = this.userService.getUser();
  }

}
