import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GlobalService } from '../global.service';

@Component({
  selector: 'app-logout',
  template: 'You logged out !',
})
export class LogoutComponent implements OnInit {

  constructor(
    public global: GlobalService,
    private router: Router
  ) {
  }

  ngOnInit(): void {
    this.global.isLogged = false;
    this.router.navigate([ '/login' ]);
  }

}
