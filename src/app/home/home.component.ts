import { Component, OnInit } from '@angular/core';
import { Vehicle } from '../Model/Vehicle';
import { VehicleListService } from '../service/vehicle/vehicle-list.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html'
})
export class HomeComponent implements OnInit {
  vehiclesForwarded: Vehicle[];

  constructor(
    private vehicleService: VehicleListService
  ) {
  }

  ngOnInit(): void {
    this.vehicleService.getVehicleList().subscribe(data => {
      this.processVehicleForwarded(data.results[0].vehicles.items);
    });
  }

  private processVehicleForwarded(items): void {
    this.vehiclesForwarded = new Array<Vehicle>();
    items.forEach(e => this.vehiclesForwarded.push(e));
  }
}
