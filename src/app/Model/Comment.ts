import { User } from './User';

export interface Comment {
  date: string;

  comment: string;

  score: number;

  upvote: number;

  user: User;
}
