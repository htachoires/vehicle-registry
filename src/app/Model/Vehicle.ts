export interface Vehicle {
  id: number;

  brand: string;

  model: string;

  score: number;

  power: number;

  nbComments: number;

  pictures: string[];
}
