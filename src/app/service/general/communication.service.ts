import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class CommunicationService {

  private baseUrl = 'https://randomapi.com/api/26395f21a37e5c60a9859d535e605ba6';

  constructor(
    private http: HttpClient
  ) {
  }

  sendRequest(parameters: { [name: string]: string; }): Observable<any> {
    let httpParams = new HttpParams();
    for (const key in parameters) {
      if (parameters[key] !== null) {
        httpParams = httpParams.set(key, parameters[key]);
      }
    }
    const finalUrl = this.baseUrl + '?' + httpParams.toString();
    return this.http.get(finalUrl).pipe(catchError(err => this.handleError(err)));
  }

  private handleError(error: any): Observable<never> {
    const errMsg = error.message || 'Server error';
    console.error(errMsg); // log to console instead
    return throwError(errMsg);
  }
}
