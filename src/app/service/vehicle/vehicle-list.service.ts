import { Injectable } from '@angular/core';
import { CommunicationService } from '../general/communication.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class VehicleListService {

  constructor(
    private communicationService: CommunicationService
  ) {
  }

  getVehicleList(params = {}): Observable<any> {
    const parameters = {...{p: 'vehicle_list'}, ...params};
    return this.communicationService.sendRequest(parameters);
  }
}
