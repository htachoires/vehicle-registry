import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CommunicationService } from '../general/communication.service';

@Injectable({
  providedIn: 'root'
})
export class VehicleDetailService {

  constructor(
    private communicationService: CommunicationService
  ) {
  }

    getVehicleDetail(id: number): Observable<any> {
    const parameters = {p: 'vehicle_detail'};
    return this.communicationService.sendRequest(parameters);
  }
}
