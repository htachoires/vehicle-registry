import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BrandsService } from './brands.service';
import { ModelsService } from './models.service';

@Injectable({
  providedIn: 'root'
})
export class SearchService {

  constructor(
    private brandsService: BrandsService,
    private modelsService: ModelsService
  ) {
  }

  getModelList(): Observable<any> {
    return this.modelsService.getModelList();
  }

  getBrandList(): Observable<any> {
    return this.brandsService.getBrandList();
  }
}
