import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CommunicationService } from '../general/communication.service';

@Injectable({
  providedIn: 'root'
})
export class ModelsService {

  constructor(
    private communicationService: CommunicationService
  ) {
  }


  getModelList(): Observable<any> {
    const parameters = {p: 'vehicle_models_list'};
    return this.communicationService.sendRequest(parameters);
  }
}
