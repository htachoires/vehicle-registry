import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CommunicationService } from '../general/communication.service';

@Injectable({
  providedIn: 'root'
})
export class BrandsService {

  constructor(
    private communicationService: CommunicationService
  ) {
  }


  getBrandList(): Observable<any> {
    const parameters = {p: 'vehicle_brands_list'};
    return this.communicationService.sendRequest(parameters);
  }
}
