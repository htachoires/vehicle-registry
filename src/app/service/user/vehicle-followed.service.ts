import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CommunicationService } from '../general/communication.service';

@Injectable({
  providedIn: 'root'
})
export class VehicleFollowedService {

  constructor(
    private communicationService: CommunicationService
  ) {
  }


  getVehicleFollowed(): Observable<any> {
    const parameters = {p: 'user_vehicle_followed'};
    return this.communicationService.sendRequest(parameters);
  }
}
