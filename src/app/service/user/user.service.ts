import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CommunicationService } from '../general/communication.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(
    private communicationService: CommunicationService
  ) {
  }

  getUser(): Observable<any> {
    const parameters = {p: 'user_information'};
    return this.communicationService.sendRequest(parameters);
  }
}
