import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { ListComponent as VehicleListComponent } from './vehicle/list/list.component';
import { HomeComponent } from './home/home.component';
import { DetailComponent } from './vehicle/detail/detail.component';
import { ProfileComponent } from './user/profile.component';
import { ForgetPasswordComponent } from './forget-password/forget-password.component';
import { LogoutComponent } from './logout/logout.component';

const routes: Routes = [
  {path: 'accueil', component: HomeComponent},
  {path: '', redirectTo: 'accueil', pathMatch: 'prefix'},
  {path: 'login', component: LoginComponent},
  {path: 'logout', component: LogoutComponent},
  {path: 'forget-password/:state', component: ForgetPasswordComponent},
  {path: 'forget-password', redirectTo: 'forget-password/email'},
  {path: 'vehicle-list', component: VehicleListComponent},
  {path: 'vehicle-detail/:id', component: DetailComponent},
  {path: 'vehicle-detail', redirectTo: 'vehicle-list'},
  {path: 'profile', component: ProfileComponent},
];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forRoot(routes),
    CommonModule
  ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {
}
