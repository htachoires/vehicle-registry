import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StateService {

  stateObserver: Subject<string> = new Subject();

  constructor() {
  }

  updateState(state: string): void {
    this.stateObserver.next(state);
  }
}
