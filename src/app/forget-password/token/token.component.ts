import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { StateService } from '../state.service';

@Component({
  selector: 'app-token',
  templateUrl: './token.component.html'
})
export class TokenComponent implements OnInit {

  constructor(
    private stateService: StateService
  ) {
  }

  ngOnInit(): void {
  }

  checkToken(form: NgForm): void {
    console.log(form.value);
    this.stateService.updateState('reset');
  }
}
