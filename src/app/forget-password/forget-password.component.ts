import { Component, OnInit } from '@angular/core';
import { StateService } from './state.service';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-forget-password',
  templateUrl: './forget-password.component.html'
})
export class ForgetPasswordComponent implements OnInit {

  state = '';

  constructor(
    private stateService: StateService,
    private route: ActivatedRoute,
    private location: Location
  ) {
    this.stateService.stateObserver.subscribe(state => {
      this.state = state;
      this.location.replaceState('/forget-password/' + state);
    });

    this.route.url.subscribe(value => {
      this.stateService.updateState(value.pop().path);
    });
  }

  ngOnInit(): void {
  }

}
