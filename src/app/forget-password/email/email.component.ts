import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { StateService } from '../state.service';

@Component({
  selector: 'app-email',
  templateUrl: './email.component.html'
})
export class EmailComponent implements OnInit {

  constructor(
    private stateService: StateService
  ) {
  }

  ngOnInit(): void {
  }

  email(form: NgForm): void {
    console.log(form.value);
    this.stateService.updateState('token');
  }
}
