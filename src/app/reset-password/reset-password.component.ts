import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html'
})
export class ResetPasswordComponent implements OnInit {

  minLength = 8;

  resetForm: FormGroup;

  ngOnInit(): void {
    this.resetForm = new FormGroup({
      firstPassword: new FormControl('', [
        Validators.required,
        Validators.minLength(this.minLength),
      ]),
      secondPassword: new FormControl('', [
        Validators.required
      ])
    });
  }

  get firstPassword(): AbstractControl {
    return this.resetForm.get('firstPassword');
  }

  get secondPassword(): AbstractControl {
    return this.resetForm.get('secondPassword');
  }

  get firstPasswordValid(): boolean {
    return this.firstPassword.valid;
  }

  get secondPasswordValid(): boolean {
    this.secondPassword.setValidators([
      Validators.pattern(this.firstPassword.value),
      Validators.required
    ]);
    return this.secondPassword.valid;
  }

  forgetPassword(): void {
    if (this.resetForm.valid) {
      this.router.navigate([ '/login' ]);
    }
  }

  constructor(private router: Router) {
  }
}
