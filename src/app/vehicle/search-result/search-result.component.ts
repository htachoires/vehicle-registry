import { Component, Input, OnInit } from '@angular/core';
import { Vehicle } from '../../Model/Vehicle';
import swal from 'sweetalert';

@Component({
  selector: 'app-search-result',
  templateUrl: './search-result.component.html'
})
export class SearchResultComponent implements OnInit {

  @Input()
  vehicles: Vehicle[];

  @Input()
  disabledActions = [];

  constructor() {
  }

  ngOnInit(): void {
  }

  addVehicleFollowed(id: number): void {
    swal({
      icon: 'success',
      text: '🤩 You now followed vehicle n°' + id + '. 🤩'
    });
  }
}
