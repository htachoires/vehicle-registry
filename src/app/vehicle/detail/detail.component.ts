import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Vehicle } from 'src/app/Model/Vehicle';
import { VehicleDetailService } from 'src/app/service/vehicle/vehicle-detail.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-vehicle-detail',
  templateUrl: './detail.component.html'
})
export class DetailComponent implements OnInit {

  public vehicleDetail: Vehicle;

  public vehicleDetailObservable: Observable<any>;

  constructor(
    private vehicleDetailService: VehicleDetailService,
    private route: ActivatedRoute
  ) {
    this.route.params.subscribe(
      params => {
        this.searchVehicle(params.id);
      }
    );
  }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.vehicleDetailObservable = this.vehicleDetailService.getVehicleDetail(params.id);
      this.vehicleDetailObservable.subscribe(data => {
        this.decodeJson(data.results[0].vehicle);
      });
    });
  }

  private decodeJson(items): void {
    this.vehicleDetail = items;
  }

  private searchVehicle(id: number): void {
    this.vehicleDetailService.getVehicleDetail(id);
    window.scroll(0, 0);
  }
}
