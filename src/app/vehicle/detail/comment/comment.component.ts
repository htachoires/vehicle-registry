import { Component, Input, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Comment } from 'src/app/Model/Comment';

@Component({
  selector: 'app-comment',
  templateUrl: './comment.component.html'
})
export class CommentComponent implements OnInit {


  @Input() vehicleDetail: Observable<any>;

  public comments: Comment[];

  constructor() {
  }

  ngOnInit(): void {
    this.vehicleDetail.subscribe(data => this.decodeJson(data.results[0].vehicle.comments.items));
  }

  private decodeJson(items): void {
    this.comments = [];

    items.forEach(i => this.comments.push(i));
    this.comments.sort((n1, n2) => {
      if (n1.upvote < n2.upvote) {
        return 1;
      }

      if (n1.upvote > n2.upvote) {
        return -1;
      }

      return 0;
    });
  }
}
