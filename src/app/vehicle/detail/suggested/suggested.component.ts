import { Component, Input, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Vehicle } from 'src/app/Model/Vehicle';

@Component({
  selector: 'app-suggested',
  templateUrl: './suggested.component.html'
})
export class SuggestedComponent implements OnInit {


  @Input() vehicleDetail: Observable<any>;

  public suggested: Vehicle[];

  constructor() {
  }

  ngOnInit(): void {
    this.vehicleDetail.subscribe(data => this.decodeJson(data.results[0].suggested_vehicles.items));
  }

  private decodeJson(items): void {
    this.suggested = new Array<Vehicle>();
    items.forEach(i => this.suggested.push(i));
  }
}
