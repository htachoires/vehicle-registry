import { Component, OnInit } from '@angular/core';
import { Vehicle } from 'src/app/Model/Vehicle';
import { VehicleListService } from 'src/app/service/vehicle/vehicle-list.service';
import { Brand } from '../../Model/Brand';
import { Model } from '../../Model/Model';
import { BrandsService } from '../../service/vehicle/brands.service';
import { ModelsService } from '../../service/vehicle/models.service';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-vehicle-search',
  templateUrl: './search.component.html'
})
export class SearchComponent implements OnInit {

  public brands: Brand[];

  public models: Model[];

  public vehicleResult: Vehicle[];

  currentRate = 0;

  searchForm: FormGroup;

  public previousSearch = Date.now();

  public searchParam = {};

  constructor(
    private brandsService: BrandsService,
    private modelsService: ModelsService,
    private searchService: VehicleListService,
    private formBuilder: FormBuilder
  ) {
    this.vehicleResult = [];
    this.searchForm = this.formBuilder.group({
      brand: new FormControl(),
      model: new FormControl(),
      score: new FormControl(),
      power: new FormControl()
    });

    this.searchVehicles();

    this.onChanges();
  }

  ngOnInit(): void {
    this.brandsService.getBrandList()
      .subscribe(data => this.decodeBrandsJson(data.results[0].brands));

    this.modelsService.getModelList()
      .subscribe(data => this.decodeModelsJson(data.results[0].models));
  }

  onChanges(): void {
    this.searchForm.valueChanges.subscribe(values => {
      const res = Date.now() - this.previousSearch;
      this.searchParam = {
        brand: values.brand,
        model: values.model,
        score: values.score,
        power: values.power
      };
      if (res > 300) {
        this.searchVehicles();
        this.previousSearch = Date.now();
      }
    });
  }

  public searchVehicles(): void {
    this.searchService.getVehicleList(this.searchParam).subscribe(data => {
      this.decodeVehiclesJson(data.results[0].vehicles.items);
    });
  }

  private decodeVehiclesJson(items): void {
    this.vehicleResult = [];
    items.forEach(e => this.vehicleResult.push(e));
  }

  private decodeBrandsJson(items): void {
    this.brands = [];
    items.forEach(i => this.brands.push({
      name: i
    }));
  }

  private decodeModelsJson(items): void {
    this.models = [];
    items.forEach(i => this.models.push({
      name: i
    }));
  }

  resetSearch(): void {
    if (this.searchForm.touched && this.searchForm.dirty) {
      this.searchForm.reset();
      this.searchVehicles();
    }
  }
}
