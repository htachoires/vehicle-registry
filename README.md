# VehicleRegistry

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 10.2.0.

Created by Delpuech Tom, Douyere Arnault et Tachoires Hugo 

## Utilisation du login

Pour utiliser le login, il faut rentrer un email valide de la forme test@email.com et pour le mot de passe, juste mettre au moins 1 caractère.

## Images présentes dans l'application

Les différentes images présentes dans l'application sont toutes générées aléatoirement donc c'est normal que les images ont aucun lien avec des voitures.
